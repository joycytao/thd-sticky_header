//
//  TruncatedLabel.swift
//  StickyHeader
//
//  Created by Joy Tao on 12/13/17.
//  Copyright © 2017 Joy Tao. All rights reserved.
//

import Foundation
import UIKit

class TruncatedLabel: UILabel {
    
    var topInset: CGFloat = -4.0
    var bottomInset: CGFloat = -4.0
    var leftInset: CGFloat = 2.0
    var rightInset: CGFloat = 2.0
    
    var maximunLinesCount: Int = 0
    var displayString :NSMutableAttributedString = NSMutableAttributedString()
    
    func configureWith(_ maximunLinesCount: Int, string: NSMutableAttributedString) {
        self.maximunLinesCount = maximunLinesCount
        self.displayString = string
        
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(topInset, leftInset , bottomInset, rightInset)))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        
        // Unwrap the current graphic context you’ll use for drawing
        guard let context = UIGraphicsGetCurrentContext() else {
            return
            
        }
        
        // Flip the coordinate system
        context.textMatrix = .identity
        context.translateBy(x: 0, y: bounds.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        
        // CTFramesetterCreateWithAttributedString creates a CTFramesetter with the supplied attributed string. CTFramesetter will manage your font references and your drawing frames.
        let frameStter: CTFramesetter = CTFramesetterCreateWithAttributedString(self.displayString )
        
        // Create a path which bounds the drawing area, the entire view’s bounds in this case
        let path = CGMutablePath()
        path.addRect(bounds)
        
        // Create a CTFrame, by having CTFramesetterCreateFrame render the entire string within path.
        let frame = CTFramesetterCreateFrame(frameStter, CFRangeMake(0, self.displayString.length), path, nil)
        guard let lines = CTFrameGetLines(frame) as? [CTLine] else {
            return
        }
        
        // Create a buffer that which the origins are copied. The buffer must have at least as many elements as specified by range's length. Each CGPoint in this array is the origin of the corresponding line in the array of lines returned by CTFrameGetLines.
        var origins: [CGPoint] = Array(repeating : CGPoint.zero, count: lines.count)
        CTFrameGetLineOrigins(frame, CFRangeMake(0, 0), &origins)
        
        if lines.count == 0 {
            return
        }
        // If greater than or equal to maximun lines count, needs to insert ellipse on last line
        if lines.count >= self.maximunLinesCount {
            // The first 2 lines, draw with CTLineDraw
            for r in 0..<self.maximunLinesCount - 1 {
                let line = lines[r]
                context.textPosition = CGPoint(x: origins[r].x, y: origins[r].y)
                CTLineDraw(line, context)
            }
            
            let lastLine = lines[self.maximunLinesCount - 1]
            let ellipsisAttr: NSMutableAttributedString = NSMutableAttributedString(string: "...")
            ellipsisAttr.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSMakeRange(0, ellipsisAttr.string.count))
            let ellipsis: CTLine = CTLineCreateWithAttributedString(ellipsisAttr)
            
            // Create a truncated line with lastLine and ellipsis
            // Draw with CTLineDraw
            if let truncatedLine = CTLineCreateTruncatedLine(lastLine, Double(self.bounds.size.width - 95), CTLineTruncationType.end, ellipsis) {
                
                context.textPosition = CGPoint(x: origins[self.maximunLinesCount - 1].x, y: origins[self.maximunLinesCount - 1].y)
                CTLineDraw(truncatedLine, context)
                
            }
             return
        }
        // If less than maximun lines counts, draw with CTFrameDraw
        else {
            CTFrameDraw(frame, context)
        }
    }
    
}
