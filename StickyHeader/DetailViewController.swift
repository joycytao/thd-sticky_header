//
//  DetailViewController.swift
//  StickyHeader
//
//  Created by Joy Tao on 12/11/17.
//  Copyright © 2017 Joy Tao. All rights reserved.
//

import UIKit

struct Fonts {
    static let HelveticaNeue11 = UIFont (name: "HelveticaNeue", size: 11)
    static let HelveticaNeue12 = UIFont (name: "HelveticaNeue", size: 12)
    static let HelveticaNeueBold12 = UIFont (name: "HelveticaNeue-Bold", size: 12)
    static let HelveticaNeueItalic12 = UIFont(name: "HelveticaNeue-Italic", size: 12)
    static let HelveticaNeueMedium12 = UIFont (name: "HelveticaNeue-Medium", size: 12)
    static let HelveticaNeueLight12 = UIFont (name: "HelveticaNeue-Light", size: 12)
    static let HelveticaNeue14 = UIFont (name: "HelveticaNeue", size: 14)
    static let HelveticaNeueLight14 = UIFont (name: "HelveticaNeue-Light", size: 14)
    static let HelveticaNeueMedium14 = UIFont (name: "HelveticaNeue-Medium", size: 14)
    static let HelveticaNeueBold14 = UIFont (name: "HelveticaNeue-Bold", size: 14)
    static let HelveticaNeueItalic14 = UIFont(name: "HelveticaNeue-Italic", size: 14)
    static let HelveticaNeue16 = UIFont (name: "HelveticaNeue", size: 16)
    static let HelveticaNeueMedium16 = UIFont (name: "HelveticaNeue-Medium", size: 16)
    static let HelveticaNeueBold16 = UIFont (name: "HelveticaNeue-Bold", size: 16)
    static let HelveticaNeue18 = UIFont (name: "HelveticaNeue", size: 16)
    static let HelveticaNeueBold18 = UIFont (name: "HelveticaNeue-Bold", size: 18)
    static let HelveticaNeueBold20 = UIFont (name: "HelveticaNeue-Bold", size: 20)
    static let HelveticaNeueCondensedBold24 = UIFont (name: "HelveticaNeue-CondensedBold", size: 24)
    
}

class DynamicLabelHeightCell: UICollectionViewCell {
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var contentTopPadding: NSLayoutConstraint!
    @IBOutlet weak var contentBottomPadding: NSLayoutConstraint!
    
    var height: CGFloat {
        
        let height: CGFloat = self.contentLabel.frame.size.height
        return height + contentTopPadding.constant + contentBottomPadding.constant
    }
}

class ProductHeaderCell: DynamicLabelHeightCell {
   
    func configure() {
        
        let brand = "Dewalt"
        let productName = "20-Volt MAX Lithium-Ion Cordless Drill/Driver and Impact Combo Kit (2-Tool) with (2) Batteries 1.3Ah, Charger and Bag"
        
        let brandLabelAttr = [NSAttributedStringKey.foregroundColor: UIColor.darkGray, NSAttributedStringKey.font:  Fonts.HelveticaNeueBold14!]
        let productNameLabelAttr = [NSAttributedStringKey.foregroundColor: UIColor.darkGray, NSAttributedStringKey.font: Fonts.HelveticaNeue14!]
        
        self.contentLabel.attributedText = NSMutableAttributedString.attributedText(strings: [brand, productName], attritubes: [brandLabelAttr, productNameLabelAttr])
    }
}

class ProductDetailsCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var chevornImageView: UIImageView!
}


class ProductBulletDetailsCell: DynamicLabelHeightCell {
    
    func configure() {
        
//        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.contentLabel.attributedText = NSMutableAttributedString.attributedStringsWithBullet(list: ["Package includes 36 Energizer Max AA alkaline batteries", "Guaranteed to hold power on the shelf for 10 years", "Power Seal technology prevents potential damage from leakage"], font: Fonts.HelveticaNeue16!, color: .black, bulletColor: .black)

        if let text:NSMutableAttributedString = self.contentLabel.attributedText as? NSMutableAttributedString  {
            var newFrame:CGRect = self.contentLabel.frame
            let contentHeight: CGFloat = text.height(for: self.contentLabel.frame.size.width)
            print("&&&&&&&&&&&&& content:\(contentHeight)")
            newFrame.size.height = contentHeight
            self.contentLabel.frame = newFrame
            
        }
    }
}

class ProductSpecDetailsCell: UICollectionViewCell {
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var internetLabel: UILabel!
    @IBOutlet weak var storeSkuLabel: UILabel!
    @IBOutlet weak var storeSoSkuLabel: UILabel!
    
    func configure() {
        
        let titleAttr: [NSAttributedStringKey : Any] = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font:  Fonts.HelveticaNeueBold16!]
        let contentAttr: [NSAttributedStringKey : Any] = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: Fonts.HelveticaNeue16!]
        
        self.modelLabel.attributedText = NSMutableAttributedString.attributedText(strings: ["Model #", "DCK240C2"], attritubes: [titleAttr, contentAttr])
        self.internetLabel.attributedText = NSMutableAttributedString.attributedText(strings: ["Internet #", "204373168"], attritubes: [titleAttr, contentAttr])
        self.storeSkuLabel.attributedText = NSMutableAttributedString.attributedText(strings: ["Store SKU #", "100024563"], attritubes: [titleAttr, contentAttr])
        self.storeSoSkuLabel.attributedText = NSMutableAttributedString.attributedText(strings: ["Store SO SKU #","100128036"], attritubes: [titleAttr, contentAttr])
        
    }

}

class DetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var stickyHeader: StickyHeader!
    @IBOutlet weak var stickyHeaderHeight: NSLayoutConstraint!
    
    private var lastContentOffset: CGFloat = 0
    private var isDisplayStickyHeader: Bool = false
    
    var headerHeight: CGFloat {
        let size: CGSize = self.collectionView(self.collectionView, layout: self.collectionView.collectionViewLayout, sizeForItemAt: IndexPath(item: 0, section: 0))
        return size.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stickyHeaderHeight.constant = 0
        self.stickyHeader.contentViewHeight.constant = 0
        
//        self.collectionView.register(ProductBulletDetailsCell.self, forCellWithReuseIdentifier: "kProductBulletDetailsCell")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        
        if offset < 0 {
            
            self.stickyHeaderHeight.constant = 0
            self.isDisplayStickyHeader = false
            return
            
        }
        if (self.lastContentOffset > offset) {
            
            if (offset <= self.headerHeight) {
                
                if (self.isDisplayStickyHeader) {
                    self.stickyHeaderHeight.constant = fabs(offset)
                    self.stickyHeader.contentViewHeight.constant = fabs(offset) - 30
                    
                    if (self.stickyHeaderHeight.constant <= 30) {
                        self.stickyHeaderHeight.constant = 0
                        self.stickyHeader.contentViewHeight.constant = 0
                        
                        UIView.animate(withDuration: 0.3, animations: {
                            self.view.layoutIfNeeded()
                        }, completion: { (completed) in
                            self.collectionView.setContentOffset(.zero, animated: false)
                            
                            self.isDisplayStickyHeader = false
                            self.stickyHeader.removeShadow()
                            
                        })
                        
                    }
                }
                
                
//                if self.stickyHeaderHeight.constant == 0 {
//                    self.isDisplayStickyHeader = false
//                    self.stickyHeader.removeShadow()
//                }
            }
        }
        else {
            if (offset >= self.headerHeight) {
                print("************ offset >= STICKY_HEADER_MAX_HEIGHT")
                if (self.isDisplayStickyHeader) {
                    return
                }
                
                self.isDisplayStickyHeader = true
                self.stickyHeaderHeight.constant = self.headerHeight
                self.stickyHeader.contentViewHeight.constant = 50
                self.stickyHeader.configure()
                
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (completed) in
                    print("completed")
                    self.stickyHeader.dropShadow()
                })
            }
            else {
                if (self.isDisplayStickyHeader) {
                    self.stickyHeaderHeight.constant = self.headerHeight
                    self.stickyHeader.contentViewHeight.constant = 50

                    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {                        
                        self.view.layoutIfNeeded()
                    })
                }
            }
        }
        self.lastContentOffset = scrollView.contentOffset.y
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (section == 0) {
            return 2
        }
        else if (section == 1) {
            return 3
        }
        else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                let cell: ProductHeaderCell = collectionView.dequeueReusableCell(withReuseIdentifier: "kProductHeaderCell", for: indexPath) as! ProductHeaderCell
                cell.configure()
                return cell
            }
            else {
                let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "kCollectionViewCell2", for: indexPath)
                return cell

            }

        }
        
        else if (indexPath.section == 1) {
            let row = indexPath.row
            switch row {
            case 0:
                let cell: ProductDetailsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "kProductDetailsCell", for: indexPath) as! ProductDetailsCell
                cell.titleLabel.text = "Product Details"
                cell.titleLabel.font = Fonts.HelveticaNeueBold20
                return cell
                
            case 1:
                let cell: ProductBulletDetailsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "kProductBulletDetailsCell", for: indexPath) as! ProductBulletDetailsCell
                cell.configure()
                return cell
            case 2:
                let cell: ProductSpecDetailsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "kProductSpecDetailsCell", for: indexPath) as! ProductSpecDetailsCell
                cell.configure()
                return cell
                
            default:
                return UICollectionViewCell()
            }
        }
        else {
            let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "kCollectionViewCell2", for: indexPath)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width :CGFloat = collectionView.frame.size.width

        return CGSize(width: width, height: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width :CGFloat = collectionView.frame.size.width
        
        if (indexPath.section == 0) {
            let height: CGFloat = (indexPath.row == 0) ? 80 : 250
            return CGSize(width: width, height: height)
        }
        else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                return CGSize(width: width, height: 44)
            }
            else if (indexPath.row == 1){
                if let cell: ProductBulletDetailsCell = (collectionView.cellForItem(at: indexPath) as? ProductBulletDetailsCell) {
                    return CGSize(width: width, height: cell.height)
                }
                return CGSize(width: width, height: 200)

            }
            else {
                return CGSize(width: width, height: 104)
            }
        }
        else {
            return CGSize(width: width, height: 200)
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if (section == 0 || section == 1) {
            return 0
        }
        else {
            return 10
        }
    }
    


}
