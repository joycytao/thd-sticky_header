//
//  StckyHeader.swift
//  StickyHeader
//
//  Created by Joy Tao on 12/1/17.
//  Copyright © 2017 Joy Tao. All rights reserved.
//

import UIKit
extension UILabel {
    var numberOfVisibleLines: Int {
        let textSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(self.sizeThatFits(textSize).height))
        let charSize: Int = lroundf(Float(self.font.pointSize))
        return rHeight / charSize
    }
    
    func getArrayOfLinesInLabel() -> [String] {
        
        let text = NSString(string: self.text ?? "-- -- -- --")
        let font = UIFont (name: "HelveticaNeue-Medium", size: 12)
        let rect = self.frame
        
        let myFont = CTFontCreateWithName((font?.fontName as CFString?)!, (font?.pointSize)!, nil)
        let attStr = NSMutableAttributedString(string: text as String)
        attStr.addAttribute(NSAttributedStringKey(rawValue: String(kCTFontAttributeName)), value:myFont, range: NSRange(location: 0, length: attStr.length))
        let frameSetter = CTFramesetterCreateWithAttributedString(attStr as CFAttributedString)
        let path = CGPath(rect: CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height), transform: nil)
        let frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, nil)
        guard let lines = CTFrameGetLines(frame) as? [CTLine] else {
            return []
        }
        
        var linesArray = [String]()
        
        for line in lines {
            let lineRange = CTLineGetStringRange(line)
            let range = NSRange(location: lineRange.location, length: lineRange.length)
            let lineString = text.substring(with: range)
            linesArray.append(lineString as String)
        }
        
        return linesArray
    }
    
    func characterPosition(subString: String, withFont: UIFont = UIFont (name: "HelveticaNeue-Medium", size: 14)!) -> CGPoint? {
        
        guard let string = self.text, let range = string.range(of: subString) else {
            return nil
        }
        let prefix = String(string[..<range.lowerBound])
        let size: CGSize = prefix.size(withAttributes: [NSAttributedStringKey.font: withFont])
        return CGPoint(x: fabs(size.width), y: 0)
        
    }
}

class AppReviewView: UIView {
    
    @IBOutlet weak var floatConstraint: NSLayoutConstraint!
    @IBOutlet weak var starsView: UIImageView!

    func configure(skuRating: CGFloat) {

        let rating = max(skuRating, 0)
        let offset = 85 * min(5, rating) / 5
        self.floatConstraint.constant = offset
    }
}

@objc
protocol StickyHeaderDelegate {
    
    func resize(withProgress progress: CGFloat)
    func overflow(withPoints points: CGFloat)
}

class StickyHeader: UIView,  StickyHeaderDelegate {
    
    let ratingIconHeight: CGFloat = 15
    let ratingIconWidth: CGFloat = 85
    
    @IBOutlet weak var productLabel: TruncatedLabel!
    @IBOutlet weak var ratingView: AppReviewView!

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var bookmarkImageView: UIImageView!

    
    @IBOutlet weak var imageViewHeight:NSLayoutConstraint!
//    @IBOutlet weak var itemDescView: UITextView!
    @IBOutlet weak var productLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var ratingViewlHeight: NSLayoutConstraint!
    @IBOutlet weak var contentBottonPadding: NSLayoutConstraint!
    
    //2
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!

    
    
    func resize(withProgress progress: CGFloat) {}
    func overflow(withPoints points: CGFloat) {}
    
    func dropShadow() {
        
        self.layer.shadowColor =  UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 5, height: 5)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 5
    }
    
    func removeShadow() {
        
        self.layer.shadowOpacity = 0
        self.layer.shadowRadius = 0
    }
    
    func willExpand () {
        
        self.configure()
        
        self.ratingView.alpha = 1
        self.productLabelHeight.constant = 50
        self.ratingViewlHeight.constant = 15
        
    }
    
    func didClose (){
        
        self.ratingView.alpha = 0
        self.productLabelHeight.constant = 0
        self.ratingViewlHeight.constant = 0

        self.removeShadow()
    }
    
    
    func configure() {
        
        //TODO- configure with PIP Object

        let brand = "Dewalt"
        let productName = "20-Volt MAX Lithium-Ion Cordless Drill/Driver and Impact Combo Kit (2-Tool) with (2) Batteries 1.3Ah, Charger and Bag"
        
        let brandLabelAttr = [NSAttributedStringKey.foregroundColor: UIColor.darkGray, NSAttributedStringKey.font:  UIFont (name: "HelveticaNeue-Medium", size: 12) as Any]
        let productNameLabelAttr = [NSAttributedStringKey.foregroundColor: UIColor.darkGray, NSAttributedStringKey.font: UIFont (name: "HelveticaNeue-Light", size: 12) as Any]
        
        self.productLabel.configureWith(3, string: NSMutableAttributedString.attributedText(strings: [brand, productName], attritubes: [brandLabelAttr, productNameLabelAttr]))
        self.ratingView.configure(skuRating: 3.5)

        
    }
    func layoutProductLabel(brand: String, productName: String) {
        
        let brandLabelAttr: [NSAttributedStringKey : Any] = [NSAttributedStringKey.foregroundColor: UIColor.darkGray, NSAttributedStringKey.font:  UIFont (name: "HelveticaNeue-Medium", size: 12) as Any]
        let productNameLabelAttr: [NSAttributedStringKey : Any] = [NSAttributedStringKey.foregroundColor: UIColor.darkGray, NSAttributedStringKey.font: UIFont (name: "HelveticaNeue-Light", size: 12) as Any]
        
        let formattedBrand = brand + " "
        self.productLabel.attributedText = NSMutableAttributedString.attributedText(strings: [formattedBrand, productName], attritubes: [brandLabelAttr, productNameLabelAttr])
        
    }
}
