//
//  ViewController.swift
//  StickyHeader
//
//  Created by Joy Tao on 11/30/17.
//  Copyright © 2017 Joy Tao. All rights reserved.
//

import UIKit

let STICKY_HEADER_MAX_HEIGHT: CGFloat = 80

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var stickyHeader: StickyHeader!
    @IBOutlet weak var stickyHeaderHeight: NSLayoutConstraint!

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewTopLayoutConstraint: NSLayoutConstraint!

    private var lastContentOffset: CGFloat = 0
    private var isDisplayStickyHeader: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.stickyHeaderHeight.constant = 0
        self.stickyHeader.productLabelHeight.constant = 0
        self.stickyHeader.ratingViewlHeight.constant = 0
        

        
//        self.stickyHeader.ratingViewlHeight.constant = 85
//        self.stickyHeader.productLabel.text = "edodjiewodiowejdoiwejdojweojdowjdowjdowdowjxnon nzcxmnsmncowjefowe nzcxmnsmncdodjiewodiowejdoiwejdododjiewodiowejdoiwejdododjiewodiowejdoiwejdododjiewodiowejdoiwejdo"
        

        
    }
    
    func animateStickyHeaderView(shouldShow: Bool, isAnimated: Bool) {
        
        if (shouldShow) {
            
            let duration = (isAnimated) ? 0.3 : 0
            self.stickyHeaderHeight.constant = STICKY_HEADER_MAX_HEIGHT
            self.stickyHeader.willExpand()
            
            UIView.animate(withDuration: duration, delay: 0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) in
                print("completed")
                self.stickyHeader.dropShadow()
            })
        }
        else {
            if (self.stickyHeader.productLabelHeight.constant > 0 ) {
                
                self.stickyHeader.productLabelHeight.constant = self.stickyHeaderHeight.constant - 30
                self.stickyHeader.ratingViewlHeight.constant = self.stickyHeader.ratingViewlHeight.constant - 15
                if (self.stickyHeader.productLabelHeight.constant <= 0) {
                    self.stickyHeader.productLabelHeight.constant = 0
                }
                if (self.stickyHeader.ratingViewlHeight.constant <= 0) {
                    self.stickyHeader.ratingViewlHeight.constant = 0
                }
            }
            else {
                self.stickyHeader.productLabelHeight.constant = 0
                self.stickyHeaderHeight.constant = 0
            }
            
            self.stickyHeader.layoutSubviews()
            
            if self.stickyHeaderHeight.constant == 0 {
                self.stickyHeader.didClose()
                
                self.collectionView.setContentOffset(.zero, animated: false)
            }

        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
       
        if offset < 0 {
            
            self.stickyHeader.didClose()
            self.isDisplayStickyHeader = false
            return

        }
        
        if (self.lastContentOffset > offset) {

            if (offset <= STICKY_HEADER_MAX_HEIGHT) {
                self.stickyHeaderHeight.constant = fabs(offset)
                self.animateStickyHeaderView(shouldShow: false, isAnimated: true)
                self.isDisplayStickyHeader = false

            }
        }
        else if (self.lastContentOffset < offset) {
            print("************ move down ************")

            if (offset >= STICKY_HEADER_MAX_HEIGHT) {
                print("************ offset >= STICKY_HEADER_MAX_HEIGHT")
                
                self.isDisplayStickyHeader = true
                self.animateStickyHeaderView(shouldShow: true, isAnimated: true)
            }
            else {
                if (self.isDisplayStickyHeader) {
                    print("************ isDisplayStickyHeader")

                    self.animateStickyHeaderView(shouldShow: true, isAnimated: true)
                }
            }
            
        }
        
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
        
        
//        if offset < 0 {
//            self.stickyHeader.didClose()
//            self.isDisplayStickyHeader = false
//            return
//        }
    
        /*
        if offset >= STICKY_HEADER_MAX_HEIGHT {
 
            print("#################### expanded\(fabs(offset))")

            if (self.isDisplayStickyHeader == true && self.stickyHeaderHeight.constant == STICKY_HEADER_MAX_HEIGHT) {
                return
            }
            self.isDisplayStickyHeader = true
            
            self.stickyHeaderHeight.constant = STICKY_HEADER_MAX_HEIGHT
            self.stickyHeader.willExpand()
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()

            }, completion: { (completed) in
                print("completed")
                self.stickyHeader.dropShadow()
            })
        }
      
        if (offset <= STICKY_HEADER_MAX_HEIGHT && self.isDisplayStickyHeader == true) {
            
            print("#################### closeing\(fabs(offset))")
            
            self.stickyHeaderHeight.constant = fabs(offset)
            
            if (self.stickyHeader.productLabelHeight.constant > 0 ) {

                self.stickyHeader.productLabelHeight.constant = self.stickyHeaderHeight.constant - 30
                self.stickyHeader.ratingViewlHeight.constant = self.stickyHeader.ratingViewlHeight.constant - fabs(offset)

                if (self.stickyHeader.productLabelHeight.constant <= 0) {
                    self.stickyHeader.productLabelHeight.constant = 0
                }
                if (self.stickyHeader.ratingViewlHeight.constant <= 0) {
                    self.stickyHeader.ratingViewlHeight.constant = 0
                }
            }
            else {
                self.stickyHeader.productLabelHeight.constant = 0
            }
            
            self.stickyHeader.layoutSubviews()
            
            if self.stickyHeaderHeight.constant == 0 {
                self.stickyHeader.didClose()
                self.isDisplayStickyHeader = false
            }
        }*/
        
    }
    
    
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if (!decelerate) {
//            if (self.isDisplayStickyHeader) {
//                self.stickyHeader.ratingView.alpha = 1
//            }
//        }
//        else {
//            if (self.isDisplayStickyHeader) {
//                self.stickyHeader.ratingView.alpha = 0
//            }
//        }
//    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "kCollectionViewCell", for: indexPath)
//        if (indexPath.row % 3 == 0) {
//            cell.backgroundColor = .blue
//        }
//        else if (indexPath.row % 3 == 1) {
//            cell.backgroundColor = .red
//        }
//        else if (indexPath.row % 3 == 2) {
//            cell.backgroundColor = .green
//        }
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width :CGFloat = collectionView.frame.size.width

        if (indexPath.row == 0) {
//            let height: CGFloat = (self.stickyHeaderHeight.constant == 0) ? 80 : 0
            
            return CGSize(width: width, height: 80)
        }
        else {
            return CGSize(width: width, height: 200)

        }
    }


}

