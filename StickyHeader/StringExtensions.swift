//
//  StringExtensions.swift
//  StickyHeader
//
//  Created by Joy Tao on 12/13/17.
//  Copyright © 2017 Joy Tao. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString  {
 
    // Label with different text attributes
    class func attributedText(strings: [String], attritubes: [[NSAttributedStringKey : Any]]) -> NSMutableAttributedString {
        
        let fullText = NSMutableAttributedString()
        for (index, string) in strings.enumerated() {
            let formattedString = "\(string) "
            let attributedString = NSMutableAttributedString(string: formattedString)
            let attribute: [NSAttributedStringKey : Any] = attritubes[index]
            attributedString.addAttributes(attribute, range: NSMakeRange(0, attributedString.length))
            fullText.append(attributedString)
        }
        
        return fullText
    }
    
    // Label with bullet symbol
    class func attributedStringsWithBullet(list: [String],
                                           font: UIFont,
                                           color: UIColor,
                                           indentation: CGFloat = 20,
                                           lineSpacing: CGFloat = 2,
                                           paragraphSpacing: CGFloat = 8,
                                           bullet: String = "\u{2022}",
                                           bulletColor: UIColor) -> NSMutableAttributedString {
        
        let textAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: color]
        let bulletAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: bulletColor]
        
        let paragraphStyle = NSMutableParagraphStyle()
        let nonOptions = [NSTextTab.OptionKey: Any]()
        paragraphStyle.tabStops = [
            NSTextTab(textAlignment: .left, location: indentation, options: nonOptions)]
        paragraphStyle.defaultTabInterval = indentation
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.paragraphSpacing = paragraphSpacing
        paragraphStyle.headIndent = indentation
        
        let bulletList = NSMutableAttributedString()
        for (index, string) in list.enumerated() {
            let formattedString = (index == list.count - 1) ? "\(bullet)\t\(string)" : "\(bullet)\t\(string)\n"
            print("\(formattedString)")
            
            let attributedString = NSMutableAttributedString(string: formattedString)
            
            attributedString.addAttributes(
                [NSAttributedStringKey.paragraphStyle : paragraphStyle],
                range: NSMakeRange(0, attributedString.length))
            
            attributedString.addAttributes(
                textAttributes,
                range: NSMakeRange(0, attributedString.length))
            
            let string:NSString = NSString(string: formattedString)
            let rangeForBullet:NSRange = string.range(of: bullet)
            attributedString.addAttributes(bulletAttributes, range: rangeForBullet)
            bulletList.append(attributedString)
        }
        
        return bulletList
        
    }
    
    
    func height(for width: CGFloat) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], context: nil)
        return actualSize.height
    }
}
